# Physical dashboard

## Hardware

* [AZDelivery D1 Mini NodeMCU Lua avec ESP8266-12F Module WLAN CH340G](https://www.amazon.fr/gp/product/B0754N794H) - 13,99 €
* [DC 5V 24 Bits Anneau LED RGB, WS2812 5050 LED Ring for Arduino Raspberry Pi LED Display, avec Pilotes Intégrés](https://www.amazon.fr/gp/product/B08DD7P8K2) - 8,77 €
* [QIQN WS2812 Led Ring 5050 RGB DC 5V 12 Bits Anneau Lumineux avec Pilotes Intégrés (3 Pièces)](https://www.amazon.fr/gp/product/B097924B4N) - 11,69 €
* [YUNBO Ruban LED WS2812B 144 Pixels 1M RGB Adressable Individuellemen CC 5V Flexible Blanc PCB IP20 Non Étanche 5050 Bande LED](https://www.amazon.fr/gp/product/B07Q9KCC6M) - 18,99 €


## Preparing development env


#### Installing pyhton3


#### Installing pip

```sh
sudo apt update
sudo apt install python3-pip
```

### Installing virtualenv

```sh
pip3 install virtualenv
```

### Create virtualenv in new folder

```sh
mkdir esp
cd esp

# Create virtual env
python3 -m virtualenv venv

# Activate virtualenv
source venv/bin/activate
```

---
## Prerparing ESP8266 with micropython

Procedure tested on Ubuntu 18.04

### Install esptool

Esptool is used to write the microphython rom tom the esp devices. From the new folder, with activated virtualenv.

```sh
pip install esptool
```
### Downlaod the last firmware

Visit the page https://micropython.org/download/esp8266/ and get the last firmware

```sh
wget https://micropython.org/resources/firmware/esp8266-20220117-v1.18.bin
```

### Erase the Esp8266

```sh
esptool.py --port /dev/ttyUSB0 erase_flash
```

### Flash the device esp with the firmware

```sh
esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20220117-v1.18.bin
```

After this steps you should be able to connect to the microphython console of the ESP board.

---
## Communication with the ESP Board

Various programs will permit you to comunicate with the ESP Board. In this part we will see how to use

* picocom (simple communication over com port with the REPL)
* RSHELL (complete communication solution with editing, and uploading files and folders)
* ampy (a rshell like communication solution from adafruit)


### Connecting to the REPL Command line with PICOCOM

```sh
 picocom /dev/ttyUSB0 -b 115200 
```


### RSHELL

From the new folder, with activated virtualenv

#### Install 
```sh
pip install rshell
```

#### Usefull commands

```sh
# Connect to the board
rshell -p /dev/ttyUSB0
```

Once rshell promt displayed

```sh
# view files from board
ls /pyboard

# copy file
cp ./local/file /pyboard/on_board_file

# copy folder
cp -r ./local/folder/ /pyboard/

# On board edit
edit /pyboard/main.py

```

### AMPY (Adafruit)

#### install
With this Adafruit tool you will be able to access filesystem of the ESP

```sh
pip install adafruit-ampy
```

#### Usefull commands

```sh
# list all files
ampy --port /dev/ttyUSB0 ls

# Get a file from esp
ampy --port /dev/ttyUSB0 get boot.py
```


---

## Precompiling py files to mpy

### Install mpy-cross

```sh
pip install mpy-cross
```

### Check version

```sh
mpy-cross --version
```

### Check of mpy implementation on device

From the device in REPL mode

```python
import sys
sys.implementation.mpy & 0xff
```


---

## Usefull REPL Commands

### Checking the firmware integrity

```python
>>> import esp
>>> esp.check_fw() 
```


### Restart esp

```python
import machine; machine.reset()
```


### Start captive portal in REPL

from app.libs.wifi_manager import WifiManager
wm = WifiManager()
wm.connect()


### Neopixel usage

```python
import machine, neopixel

gpio_pin = 4
number_of_leds = 8
np = neopixel.NeoPixel(machine.Pin(gpio_pin), number_of_leds)

np[0] = (255, 0, 0) # set to red, full brightness
np[1] = (0, 128, 0) # set to green, half brightness
np[2] = (0, 0, 64)  # set to blue, quarter brightness

np.write()
```

## Makefile

### configuration

Prepare script with env vars

```bash
DEVICE=/dev/ttyUSB0
ARCHITECTURE=xtensa
FIRMWARE=firmware/esp8266-20220618-v1.19.1.bin
```

### Makefile Commands

* compile: createdist

First look on create dist step. Compile command will apply mpy-cross on all py files, and removes original python files from dist repository

* createdist

Copy all src/ source to dist/ folder, excluding pyhton cache files

* clean

Removes totaly the dist folder

* flash: erase
* erase

Flash and erase commands are commands to install micropython on the esp with the firmware. The firmware that will be installed is specifyed in FIRMWARE env var.

* test

Run local python tests

* upload

Upload the application folder (src/app/) and main.py file. The upload uses rsync method for the uploading. So only modified files will be uploaded

* repl

Launch the repl mode. You'll be able to use pyhton promt

* rshell

Launch rshell promt.

* creds_reset

Specific to the project helper. Removes the wifi cred file so the server can restart from scrach again.



---

## Draft notes

### Build / Compile firmware from sources

https://learn.adafruit.com/building-and-running-micropython-on-the-esp8266/build-firmware

### Software sources

https://ansonvandoren.com/posts/esp8266-captive-web-portal-part-1/


---

## TODO List:

