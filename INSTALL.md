# Installing dev env procedure

Installing the environment for develop

## Create virtualenv

```sh
python3 -m virtualenv venv
```


## Activate venv
```sh
source venv/bin/activate
```


## Install needed packages

```sh
pip install -r requirements.txt
```
