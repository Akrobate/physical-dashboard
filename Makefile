DEVICE=/dev/ttyUSB0
ARCHITECTURE=xtensa
FIRMWARE=firmware/esp8266-20220618-v1.19.1.bin

# Prepare code (copy / compile / clean)

compile: createdist
	find ./dist/app -type f -name "*.py" -exec mpy-cross -march=$(ARCHITECTURE) {} \;
	find ./dist/app -type f -name "*.py" -exec rm {} \;


createdist:
#	cp -r src dist
	rsync -avh  ./src/ ./dist --delete
	find ./dist -type d -name "__pycache__" -exec rm -rf {} +


clean:
	rm dist -rf


# Flash firmware on the device (flash with firmware / erase the device)

flash: erase
	esptool.py --port $(DEVICE) --baud 460800 write_flash --flash_size=detect 0 $(FIRMWARE)


erase:
	esptool.py --port $(DEVICE) erase_flash


# Standard python unittests run

test:
	PYTHONDONTWRITEBYTECODE=1 python -m unittest discover src "test_*.py"


# RSHELL Section (REPL / Upload / Rshel)

upload:
	echo $(DEVICE)
	rshell -p $(DEVICE) rsync -m ./dist/app /pyboard/app
	rshell -p $(DEVICE) cp ./dist/main.py /pyboard/main.py


repl:
	rshell -p $(DEVICE) repl

	
rshell:
	rshell -p $(DEVICE)


creds_reset:
	rshell -p $(DEVICE) rm /pyboard/wifi.creds