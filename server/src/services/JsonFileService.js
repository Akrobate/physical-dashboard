'use strict';

const fsp = require('fs').promises;

class JsonFileService {

    /**
     * @return {string}
     */
    static get DATA_FOLDER() {
        return './data';
    }

    /**
     * @returns {DataSerive}
     */
    static getInstance() {
        if (JsonFileService.instance === null) {
            JsonFileService.instance = new JsonFileService();
        }
        return JsonFileService.instance;
    }


    /**
     * @param {string} credential
     * @return {Promise}
     */
    async getFileData(credential) {
        const file_string = await fsp.readFile(`${this.getDataFolder()}/${credential}.json`);
        return JSON.parse(file_string);
    }


    /**
     * @param {string} credential
     * @param {Object} data
     * @return {Promise}
     */
    async saveDataFile(credential, data) {
        await fsp.writeFile(
            `${this.getDataFolder()}/${credential}.json`,
            JSON.stringify(data)
        );
        return data;
    }


    /**
     * @returns {String}
     */
    getDataFolder() {
        return JsonFileService.DATA_FOLDER;
    }

}


JsonFileService.instance = null;

module.exports = {
    JsonFileService,
};
