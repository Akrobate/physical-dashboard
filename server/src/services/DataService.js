'use strict';

const {
    CacheService,
} = require('./CacheService');

const {
    JsonFileService,
} = require('./JsonFileService');

class DataService {

    /**
     * @param {JsonFileService} json_file_service
     * @param {CacheService} cache_service
     * @return {DataService}
     */
    constructor(
        json_file_service,
        cache_service
    ) {
        this.json_file_service = json_file_service;
        this.cache_service = cache_service;
    }


    /* istanbul ignore next */
    /**
     * @returns {DataSerive}
     */
    static getInstance() {
        if (DataService.instance === null) {
            DataService.instance = new DataService(
                JsonFileService.getInstance(),
                CacheService.getInstance()
            );
        }
        return DataService.instance;
    }


    /**
     * @param {string} credential
     * @param {Object} data
     * @return {Promise}
     */
    async saveDataFileAndUpdateCache(credential, data) {
        await this.json_file_service.saveDataFile(credential, data);
        await this.cacheUpdate(credential);
    }


    /**
     * @param {string} credential
     * @return {Promise}
     */
    async getCachedFile(credential) {
        if (!this.cache_service.existInCache(credential)) {
            await this.cacheUpdate(credential);
        }
        return this.cache_service.getFromCache(credential);
    }


    /**
     * @param {string} credential
     * @return {Promise}
     */
    async cacheUpdate(credential) {
        const data = await this.json_file_service.getFileData(credential);
        this.cache_service.setInCache(credential, data);
    }

}


DataService.instance = null;

module.exports = {
    DataService,
};
