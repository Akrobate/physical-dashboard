'use strict';


class CacheService {

    /**
     * @return {CacheService}
     */
    constructor() {
        this.cache = {};
    }


    /* istanbul ignore next */
    /**
     * @returns {DataSerive}
     */
    static getInstance() {
        if (CacheService.instance === null) {
            CacheService.instance = new CacheService();
        }
        return CacheService.instance;
    }


    /**
     * @param {string} key
     * @return {Promise}
     */
    getFromCache(key) {
        return this.cache[key];
    }


    /**
     * @param {string} key
     * @param {Any} data
     * @return {Promise}
     */
    setInCache(key, data) {
        this.cache[key] = data;
    }


    /**
     * @param {string} key
     * @returns {Bool}
     */
    existInCache(key) {
        return (this.cache[key] !== undefined);
    }

}


CacheService.instance = null;

module.exports = {
    CacheService,
};
