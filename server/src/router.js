'use strict';

const {
    Router,
} = require('express');
const router = new Router();

const {
    DataService,
} = require('./services');

const data_service = DataService.getInstance();


router.get('/ping', (request, result) => {
    result
        .status(200)
        .json({
            status: 'ok',
        });
});

router.get('/data/:credential', async (request, result) => {
    const data = await data_service.getCachedFile(request.params.credential);
    result
        .status(200)
        .json(data);
});

router.post('/data/:credential', async (request, result) => {
    const data = await data_service
        .saveDataFileAndUpdateCache(request.params.credential, request.body);
    result
        .status(201)
        .json(data);
});

router.get('/data/:credential/update-cache', async (request, result) => {
    const data = await data_service.cacheUpdate(request.params.credential);
    result
        .status(200)
        .json(data);
});

module.exports = router;
