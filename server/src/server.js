'use strict';

const app = require('./app');

const port = 5643;

app.listen(port, () => {
    console.log(`Server running on ${port}`);
});
