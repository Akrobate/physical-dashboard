'use strict';

const HTTP_CODE = require('http-status');
const superTest = require('supertest');
const {
    expect,
} = require('chai');
const {
    stub,
} = require('sinon');

const {
    JsonFileService,
} = require('../../src/services/JsonFileService');
const {
    DataService,
} = require('../../src/services/DataService');

const json_file_service = JsonFileService.getInstance();

const app = require('../../src/app');
const superApp = superTest(app);

describe('Route data by credential', () => {

    const stubs = {};

    beforeEach(() => {
        
        DataService.instance.cache = {};
        
        stubs.getDataFolder = stub(json_file_service, 'getDataFolder')
            .callsFake(() => './test/seeds/data');
    });

    afterEach(() => {
        stubs.getDataFolder.restore();
    });


    it('Simple get data by credential', async () => {
        await superApp
            .get('/v1/data/TEST_DATA_FILE_CREDENTIAL')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
            });
    });


    it('Second time should income from cache', async () => {
        await superApp
            .get('/v1/data/TEST_DATA_FILE_CREDENTIAL')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
            });

        await superApp
            .get('/v1/data/TEST_DATA_FILE_CREDENTIAL')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
            });
    });
});


