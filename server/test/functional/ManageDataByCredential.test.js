'use strict';

const fs = require('fs');
const HTTP_CODE = require('http-status');
const superTest = require('supertest');
const {
    expect,
} = require('chai');

const app = require('../../src/app');
const superApp = superTest(app);
const test_credential_key = 'TEST_DATA_FILE_CREDENTIAL';
const test_credential_data = {
    key_1: 'value_1',
    key_2: 'value_2',
    key_3: 'value_3',
};

const file_path = `./data/${test_credential_key}.json`;

describe('Manage data by credential', () => {

    beforeEach(() => {
        if (fs.existsSync(file_path)) {
            fs.unlinkSync(file_path);
        }
    });


    it('Create credential data file', async () => {
        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(test_credential_data)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });

        const data = JSON.parse(fs.readFileSync(file_path));
        expect(data).to.deep.equal(test_credential_data);
    });


    it('Create credential data file and read it', async () => {
        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(test_credential_data)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });

        await superApp
            .get(`/v1/data/${test_credential_key}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.deep.equal(test_credential_data);
            });
    });


    it('Create credential data file and read it, update it, and read again', async () => {

        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(test_credential_data)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });


        expect(
            JSON.parse(fs.readFileSync(file_path))
        ).to.deep.equal(test_credential_data);

        await superApp
            .get(`/v1/data/${test_credential_key}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.deep.equal(test_credential_data);
            });


        const update_seed = {
            ...test_credential_data,
            'key_5': 'value_5',
        };

        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(update_seed)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });

        expect(
            JSON.parse(fs.readFileSync(file_path))
        ).to.deep.equal(update_seed);

        await superApp
            .get(`/v1/data/${test_credential_key}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.deep.equal(update_seed);
            });
    });

    // @todo repository include file update cache
    it('Create credential data file and read it, update the file, force update cache, and read again', async () => {

        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(test_credential_data)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });


        expect(
            JSON.parse(fs.readFileSync(file_path))
        ).to.deep.equal(test_credential_data);

        await superApp
            .get(`/v1/data/${test_credential_key}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.deep.equal(test_credential_data);
            });


        const update_seed = {
            ...test_credential_data,
            'key_5': 'value_5',
        };

        await superApp
            .post(`/v1/data/${test_credential_key}`)
            .send(update_seed)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
            });

        expect(
            JSON.parse(fs.readFileSync(file_path))
        ).to.deep.equal(update_seed);

        await superApp
            .get(`/v1/data/${test_credential_key}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.deep.equal(update_seed);
            });
    });


});


