'use strict';

const HTTP_CODE = require('http-status');
const superTest = require('supertest');
const {
    expect,
} = require('chai');

const app = require('../../src/app');
const superApp = superTest(app);

describe('Route ping', () => {

    it('Simple get call', async () => {
        await superApp
            .get('/v1/ping')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('status', 'ok');
            });
    });
});


