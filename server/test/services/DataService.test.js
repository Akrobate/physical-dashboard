'use strict';

const {
    expect,
} = require('chai');

const {
    stub,
} = require('sinon');

const {
    JsonFileService,
} = require('../../src/services/JsonFileService');

const json_file_service = JsonFileService.getInstance();


describe('JsonFileService', () => {

    const stubs = {};

    beforeEach(() => {
        stubs.getDataFolder = stub(json_file_service, 'getDataFolder')
            .callsFake(() => './test/seeds/data');
    });


    afterEach(() => {
        stubs.getDataFolder.restore();
    });


    it('Should be able to read data', async () => {
        const data = await json_file_service.getFileData('TEST_DATA_FILE_CREDENTIAL');
        expect(data).to.be.an('Object');
    });

});
