# Server physical board

## Process

* physical board has an id
* server has to ask (the user via UI) the physical board id
* when pairing is done somes files are made available


```js
const fs = require('fs');
require('log-timestamp');

const buttonPressesLogFile = './button-presses.log';

console.log(`Watching for file changes on ${buttonPressesLogFile}`);

fs.watch(buttonPressesLogFile, (event, filename) => {
  if (filename && event ==='change') {
    console.log(`${filename} file Changed`);
  }
});
```