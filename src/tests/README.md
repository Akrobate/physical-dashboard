# Running tests on the micropython app

## From the src folder run

```sh
PYTHONDONTWRITEBYTECODE=1 python -m unittest tests/test_*.py
```

## Discovery and run test from the root folder run
```sh
YTHONDONTWRITEBYTECODE=1 python -m unittest discover src "test_*.py"
```