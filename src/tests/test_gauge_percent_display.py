import unittest

from app.gauge import GaugePercentDisplay

class TestGaugePercentDisplay(unittest.TestCase):

    def test_initialState(self):
        gauge_display = GaugePercentDisplay(2)
        self.assertEqual(gauge_display.getPointsCount(), 2)
        self.assertEqual(gauge_display.getPointData(), [])


    def utilitary_helper(self, points_count, percentage, count_on, count_off):
        color = (64, 64, 64)
        no_color = (0, 0, 0)
        gauge_display = GaugePercentDisplay(points_count)
        gauge_display.setPercentage(percentage)
        self.assertEqual(
            gauge_display.getPointData(),
            [
                *list(map(lambda _: color, list(range(count_on)))),
                *list(map(lambda _: no_color, list(range(count_off))))
            ]
        )


    def test_50Percent(self):
        self.utilitary_helper(points_count = 2, percentage = 50, count_on = 1, count_off = 1)


    def test_50PercentOn9Points(self):
        self.utilitary_helper(points_count = 9, percentage = 50, count_on = 5, count_off = 4)


    def test_smallValue(self):
        self.utilitary_helper(points_count = 10, percentage = 1, count_on = 1, count_off = 9)


    def test_bigValue(self):
        self.utilitary_helper(points_count = 10, percentage = 99, count_on = 10, count_off = 0)


    def test_smallFloatValue(self):
        self.utilitary_helper(points_count = 10, percentage = 0.001, count_on = 1, count_off = 9)


    def test_smallZeoValue(self):
        self.utilitary_helper(points_count = 10, percentage = 0, count_on = 0, count_off = 10)


if __name__ == '__main__':
    unittest.main()