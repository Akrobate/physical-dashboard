import unittest

# from app.gauge.gauge_pie_display import GaugePieDisplay
from app.gauge import GaugePieDisplay

W = (255, 255, 255)
R = (255, 0, 0)
G = (0, 255, 0)
B = (0, 0, 255)
V = (255, 0, 255)
NC = (0, 0, 0)


class TestGaugePieDisplay(unittest.TestCase):

    def utilitary_helper(self, points_count, pie_data, color_list):
        gauge_display = GaugePieDisplay(points_count)
        gauge_display.setPieData(pie_data)
        self.assertEqual(
            gauge_display.getPointData(),
            color_list
        )

    def test_updatePointData(self):
        self.utilitary_helper(
            5, 
            [
                {
                    'value': 1,
                    'color': W,
                },
                {
                    'value': 10,
                    'color': R,
                },
                {
                    'value': 20,
                    'color': G,
                },
                {
                    'value': 30,
                    'color': B,
                }
            ], 
            [R, G, G, B, B]
        )


    def test_updatePointData_1(self):
        self.utilitary_helper(
            3,
            [
                {
                    'value': 1,
                    'color': W,
                },
                {
                    'value': 1,
                    'color': R,
                },
                {
                    'value': 10,
                    'color': G,
                },
                {
                    'value': 10,
                    'color': B,
                }
            ], 
            [G, G, B]
        )


    def test_updatePointData_1(self):
        self.utilitary_helper(
            10,
            [
                {
                    'value': 10,
                    'color': G,
                },
                {
                    'value': 10.3,
                    'color': B,
                }
            ], 
            [G, G, G, G, G, B, B, B, B, B]
        )

    def test_updatePointData_1(self):
        self.utilitary_helper(
            5,
            [], 
            [NC, NC, NC, NC, NC]
        )



if __name__ == '__main__':
    unittest.main()