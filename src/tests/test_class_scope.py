import unittest


class ClassA():

    def __init__(self):
        self.iam = "classA"
        self.class_b = ClassB()
    
    
    def inject(self):
        self.class_b.set_class_a_function(self.coucou)


    def coucou(self, param):
        print("In coucouc")
        print("Iam %s" % self.iam)
        print("Param %s" % param)
        print("Coucou from A")

    def exec(self):
        self.class_b.execute_class_a_function()


class ClassB():

    def __init__(self):
        
        self.iam = "classB"
        self.class_a_function = None
        print("ClassB inited")

    def set_class_a_function(self, class_a_function):
        self.class_a_function = class_a_function


    def execute_class_a_function(self):
        return self.class_a_function("ParamfromB")
    



class TestClassScope(unittest.TestCase):

    def test_scope(self):
        class_a = ClassA()
        class_a.inject()
        class_a.exec()

        self.assertEqual(2, 2)
        

if __name__ == '__main__':
    unittest.main()