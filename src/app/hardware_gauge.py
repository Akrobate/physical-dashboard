import machine
import neopixel

class HardwareGauge:
    def __init__(self, pin, gauge_display):
        self.pin = pin
        self.gd = gauge_display
        self.np = neopixel.NeoPixel(machine.Pin(pin), gauge_display.getPointsCount())

    def update(self):
        for idx, value in enumerate(self.gd.getData()):
            self.np[idx] = value
        self.np.write()
