from .server_http import HTTPServer


class CaptiveServer():

    def __init__(self):
        self.http_server = HTTPServer()

    def routes(self):
        self.routes = {
            #b"/": b"%s/index.html" % self.templates_path,
            b"/": self.controller_index,
            b"/login": self.controller_login,
            b"/file": self.controller_file_test,
            b"/connected": self.controller_file_test,
        }

    def start(self):
        pass

    def controller_login(self):
        pass

    def controller_index(self, params):
        body = open("%s/index.html" % self.http_server.get_template_path(), "rb").read()
        return self.sendResponse(body)


    def controller_file_test(self, params):
        body = open("%s/file.html" % self.http_server.get_template_path(), "rb").read()
        return self.sendResponse(body)

    def connected(self, params):
        body = open("%s/connected.html" % self.http_server.get_template_path(), "rb").read() % ('self.ssid', 'self.local_ip')
        return self.sendResponse(body)
