from .gauge_display import GaugeDisplay

class GaugePercentDisplay(GaugeDisplay):
    def __init__(self, points_count):
        # Generic
        self.points_count = points_count
        self.point_data = []
        self.default_color = (128, 128, 128)
        self.default_no_color = (0, 0, 0)
        self.brightness = 50

        # Specific
        self.percentage = 0

    # Specific
    def setPercentage(self, percentage):
        self.percentage = percentage
        self.updatePointData()

    # Specific
    def updatePointData(self):
        self.point_data = []
        for boundary_value in range(0, self.points_count):
            if (100 / self.points_count * boundary_value) < self.percentage:
                self.point_data.append(self.adaptColorWithBrightness(self.default_color, self.brightness))
            else:
                self.point_data.append(self.adaptColorWithBrightness(self.default_no_color, self.brightness))

