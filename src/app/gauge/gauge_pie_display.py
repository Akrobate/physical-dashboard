'''
input param
data = [
    {
        value: Integer
        color: (Integer, Integer, Integer)
    },
    {
        value: Integer
        color: (Integer, Integer, Integer)
    }
]
total = Integer (optionnal)
'''

from .gauge_display import GaugeDisplay

class GaugePieDisplay(GaugeDisplay):

    def __init__(self, points_count):
        # Generic
        self.points_count = points_count
        self.point_data = []
        self.default_color = (128, 128, 128)
        self.default_no_color = (0, 0, 0)
        self.brightness = 50

        # Specific
        self.pie_data = []


    # Specific
    def setPieData(self, pie_data):
        self.pie_data = pie_data
        self.updatePointData()


    def updatePointData(self):
        self.point_data = []

        pie_border_list = self.pieDataBorders()
        step = self.sumPieValues() / self.points_count

        for index in range(self.points_count):
            point_from = index * step
            point_to = (index + 1) * step
            candidate_list = self.searchCandidatesForPointColor(point_from, point_to, pie_border_list)
            choice = self.chooseBestCandidateColor(point_from, point_to, candidate_list)
            self.point_data.append(choice)


    def searchCandidatesForPointColor(self, point_from, point_to, pie_border_list):
        candidate_values = []
        for boundary in pie_border_list:
            # Eliminate all boundaries that does not intersect
            if (boundary['to'] < point_from) or (boundary['from'] > point_to):
                continue
            # keeps all inside and intersected
            if (boundary['from'] < point_to) or (boundary['to'] > point_from):
                candidate_values.append(boundary)
        return candidate_values


    def chooseBestCandidateColor(self, point_from, point_to, candidate_list):

        best_score = 0
        choice = self.default_no_color
        for candidate in candidate_list:
            
            _from = candidate['from']
            _to = candidate['to']

            if (candidate['from'] < point_from):
                _from = point_from

            if (candidate['to'] > point_to):
                _to = point_to

            score = _to - _from
            if (score > best_score):
                best_score = score
                choice = candidate['color']

        return choice


    # Calculate theorical borders
    def pieDataBorders(self):
        border_list = []
        last_value = 0
        for pie_quarter in self.pie_data:
            border_list.append(
                {
                    'from': last_value,
                    'to': last_value + pie_quarter['value'],
                    'color': pie_quarter['color']
                }
            )
            last_value = last_value + pie_quarter['value']
        return border_list


    # Specific
    def sumPieValues(self):
        return sum(map(lambda x: x['value'], self.pie_data))
