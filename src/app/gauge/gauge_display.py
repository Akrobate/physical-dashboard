
class GaugeDisplay:
    def __init__(self, points_count):
        # Generic
        self.points_count = points_count
        self.point_data = []
        self.default_color = (128, 128, 128)
        self.default_no_color = (0, 0, 0)
        self.brightness = 50

    # Generic
    def getPointsCount(self):
        return self.points_count

    # Generic
    def getPointData(self):
        return self.point_data

    # Generic
    def setDefaultColor(self, color):
        self.default_color = color

    # Generic
    def setBrightness(self, brightness):
        self.brightness = brightness

    # Generic
    def adaptColorWithBrightness(self, color, brightness):
        new_color = [0, 0, 0]
        for n in range(3):
            new_color[n] = int(color[n] * (brightness / 100))
        return (new_color[0], new_color[1], new_color[2])





