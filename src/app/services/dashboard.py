import machine
import neopixel

from app.harware_gauge import HardwareGauge

from app.gauge.gauge_percent_display import GaugePercentDisplay
from app.gauge.gauge_pie_display import GaugePieDisplay

from .http_data_client import HttpDataClient

from app.configuration import NEOPIXEL_CIRCLE_24_1
from app.configuration import NEOPIXEL_CIRCLE_12_1
from app.configuration import NEOPIXEL_CIRCLE_12_2
from app.configuration import NEOPIXEL_CIRCLE_12_3
from app.configuration import NEOPIXEL_STRIP_N_VERTICAL_1
from app.configuration import NEOPIXEL_STRIP_N_VERTICAL_2
from app.configuration import NEOPIXEL_STRIP_N_VERTICAL_3

CAPTIVE_PORTAL_HTML_TEMPLATES_PATH = "./app/captive_portal/html_templates"
CAPTIVE_PORTAL_AP_IP = "192.168.4.1"


class Dashboard:

    def __init__(self):
        
        #init gauge
        #init hardware and inject
        
        pass

    def update(self):
        pass
