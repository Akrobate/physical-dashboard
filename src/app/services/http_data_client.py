import urequests
from urllib.parse import urlencode

class HttpDataClient:

    def __init__(self):
        self.url = None
        self.credential = None
        self.data = None


    def get(self, url, params=None, **kw):
        if params:
            url = url.rstrip('?') + '?' + urlencode(params, doseq=True)
        return urequests.get(url, **kw)


    def getData(self):
        response = self.get(self.url + '/data', self.credential)
        self.data = response.json()
        response.close()


    def ping(self):
        response = self.get(self.url + '/ping', self.credential)
        reponse_json = response.json()
        response.close()
        return reponse_json

