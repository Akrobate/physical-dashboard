from app.captive_portal.captive_portal import CaptivePortal

from app.configuration import CAPTIVE_PORTAL_HTML_TEMPLATES_PATH, CAPTIVE_PORTAL_AP_IP
from app.hardware_gauge import HardwareGauge
from app.gauge.gauge_percent_display import GaugePercentDisplay

from time import sleep


def start():
    # portal = CaptivePortal(
    #     local_ip=CAPTIVE_PORTAL_AP_IP,
    #     essid = 'PhysicalDashboard',
    #     templates_path = CAPTIVE_PORTAL_HTML_TEMPLATES_PATH
    # )
    # portal.start()

    test_pin = 12
    test_point_count = 16

    gauge_percent_display = GaugePercentDisplay(test_point_count)
    hardware_gauge = HardwareGauge(test_pin, gauge_percent_display)


    gauge_percent_display.setPercentage(1)
    hardware_gauge.update()
    sleep(2)

    gauge_percent_display.setPercentage(50)
    hardware_gauge.update()
    sleep(2)

    gauge_percent_display.setPercentage(100)
    hardware_gauge.update()
    sleep(2)
